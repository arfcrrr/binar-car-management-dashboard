# Binar Car Management Dashboard

This document provides guidelines and examples for Binar Rental Car Management System Web Application APIs, encouraging consistency and best practices accross applications. Binar Car APIs' main goal is to practice using a good RESTful API. 


## Database Overview

### Entity Relationship Diagram

![alt text](https://gitlab.com/arfcrrr/binar-car-management-dashboard/-/raw/develop/ERD%20-%20Binar%20Car%20Management%20System.png)

`database_binar_car` contains 1 main table `Cars` which has 7 attributes:
- id: integer, PRIMARY KEY
- name: varchar
- rent_per_day: integer
- size: varchar
- image: text
- created_at: timestamp
- updated_at: timestamp


## Run the App

    nodemon app.js


## Endpoint Overview

| Method | Route            | Description                              |
| ------ | ---------------- | ---------------------------------------  |
| GET    | /cars            | Shows list of available cars in database |
| GET    | /cars/:id        | Shows car's data based on id             |
| GET    | /cars/form       | Shows form input to add new cars' dat    |
| POST   | /cars/create     | Add new car's data to database           |
| GET    | /cars/edit/:id   | Shows form input to update car's data    |
| POST   | /cars/edit/:id   | Update car's data based on data input    |
| GET    | /cars/delete/:id | Delete car's data based on id            |


## Route List

`GET /cars`

    http://localhost:8000/cars

`GET /cars/:id`

    http://localhost:8000/cars/:id

`GET /cars/form`

    http://localhost:8000/cars/form

`POST /cars/create`

    http://localhost:8000/cars/create

`GET /cars/edit/:id`

    http://localhost:8000/cars/edit/:id

`POST /cars/edit/:id`

    http://localhost:8000/cars/edit/:id

`GET /cars/delete/:id`

    http://localhost:8000/cars/delete/:id