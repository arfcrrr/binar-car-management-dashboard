const express = require('express')
const routes = require('./routes/index.route')

const app = express();
const PORT = process.env.PORT || 8000;

app.set('view engine', 'ejs');

app.use(express.static('assets'));
app.use(express.urlencoded({ extended: true }));

app.use(routes);

app.listen(PORT, () => {
    console.log(`Server is listening on http://localhost:${PORT}`);
})