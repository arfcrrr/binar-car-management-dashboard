const express = require('express');
const req = require('express/lib/request');
const router = express.Router();

const carsRoute = require('./cars.route');

// Route for Homepage
router.get('/', (req, res) => {
    res.render('index');
});

// Route for Cars 
router.use('/cars', carsRoute);

module.exports = router;